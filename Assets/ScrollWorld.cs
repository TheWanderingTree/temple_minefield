﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollWorld : MonoBehaviour {

	public float scrollSpeed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		Vector3 newPos = new Vector3(transform.position.x, transform.position.y, transform.position.z + scrollSpeed);
		transform.position = newPos;
	}
}
