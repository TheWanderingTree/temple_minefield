﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class TestEventSubscriber : MonoBehaviour {

	private float[] choices;
	private int randomIndex;
	private float rand;
	private float startXscale;
	private float startYscale;
	private float startZscale;
	private float startXpos;
	private float startYpos;
	private float startZpos;
	private float timer;
	

	// Use this for initialization
	void Start () {
		Koreographer.Instance.RegisterForEvents( "BeatHit" , FireEvent);
		startXscale = transform.localScale.x;
		startYscale = transform.localScale.y;
		startZscale = transform.localScale.z;
		startXpos = transform.localPosition.x;
		startYpos = transform.localPosition.y;
		startZpos = transform.localPosition.z;
		//timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
		//timer += Time.deltaTime;
		//if (timer >= 0.5f) { FireEvent(); timer = 0; }
	}

	void FireEvent(KoreographyEvent fireEvent)
	{
		//Debug.Log("Event fired.");

		choices = new float[] { -1, 0, 1 };

		randomIndex = Random.Range(0, choices.Length);
		rand = choices[randomIndex];
		AnimationCurve xPos = AnimationCurve.EaseInOut(0, transform.localPosition.x, 0.1f, startXpos + rand);

		choices = new float[] { 0, 0.5f, 1 };

		randomIndex = Random.Range(0, choices.Length);
		rand = choices[randomIndex];
		AnimationCurve yPos = AnimationCurve.EaseInOut(0, transform.localPosition.y, 0.1f, startYpos + rand);

		choices = new float[] { -1, 0, 1 };

		randomIndex = Random.Range(0, choices.Length);
		rand = choices[randomIndex];
		AnimationCurve zPos = AnimationCurve.EaseInOut(0, transform.localPosition.z, 0.1f, startZpos + rand);

		choices = new float[] { 0.5f, 0.75f, 1f, 1.25f, 1.5f };

		randomIndex = Random.Range(0, choices.Length);
		rand = choices[randomIndex];
		AnimationCurve xScale = AnimationCurve.EaseInOut(0, transform.localScale.x, 0.1f, startXscale * rand);

		randomIndex = Random.Range(0, choices.Length);
		rand = choices[randomIndex];
		AnimationCurve yScale = AnimationCurve.EaseInOut(0, transform.localScale.y, 0.1f, startYscale * rand);

		randomIndex = Random.Range(0, choices.Length);
		rand = choices[randomIndex];
		AnimationCurve zScale = AnimationCurve.EaseInOut(0, transform.localScale.z, 0.1f, startZscale * rand);


		AnimationClip clip = new AnimationClip();
		clip.legacy = true;
		clip.SetCurve("",typeof(Transform), "localPosition.x", xPos);
		clip.SetCurve("",typeof(Transform), "localPosition.y", yPos);
		clip.SetCurve("",typeof(Transform), "localPosition.z", zPos);
		clip.SetCurve("",typeof(Transform), "localScale.x", xScale);
		clip.SetCurve("",typeof(Transform), "localScale.y", yScale);
		clip.SetCurve("",typeof(Transform), "localScale.z", zScale);

		Animation anim = GetComponent<Animation>();
		anim.AddClip(clip, "test");
		anim.Play("test");
		//GetComponent<Animator>().SetTrigger("Shift");
	}
}
