﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;

public class Leviathan : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Koreographer.Instance.RegisterForEvents( "FirstCue" , FireEvent);
	}
	
	// Update is called once per frame
	void FireEvent(KoreographyEvent koreoEvent)
	{
		GetComponent<Animator>().SetTrigger("Enter");
	}
}
